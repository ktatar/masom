
import numpy as np
import csv
import json
import re
import os
from scipy.stats import entropy
import itertools
import nltk
from nltk.tokenize import RegexpTokenizer


def windows(x,w):
	x = np.atleast_1d(x)
	return x[np.arange(x.shape[0]-w+1)[:,None] + np.arange(w)[None,:]]

def efficiency(x,n):
  # x is list of lists
  x = np.concatenate([windows(xx,n) for xx in x],axis=0)
  prob = np.unique(x, return_counts=True)[1]
  return entropy(prob) / np.log(len(prob))

def repetition(x,n):
  ngrams = np.concatenate([windows(xx,n) for xx in x], axis=0)
  return 1. - (float(len(np.unique(ngrams,axis=0))) / len(ngrams))

def alphabet(x):
  return len(np.unique(np.hstack(x)))

def mean_jaccard(x):
  return np.mean([jaccard(a,b) for a,b in itertools.combinations(x,2)])

def load_text_data():
  tokens = []
  d = {}
  tokenizer = RegexpTokenizer(r'\w+')
  for path in nltk.corpus.gutenberg.fileids():
    raw = nltk.corpus.gutenberg.raw(path)
    words = list(tokenizer.tokenize(raw))
    token = []
    for w in words:
      if not w in d:
        d[w] = len(d)
      token.append(d[w])
    tokens.append(token)
  return tokens

def parse_coll(filepath, key=1):
	import re
	x = {}
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > key+1:
				x[items[key]] = list(map(int,items[key+1:]))
	return x

def parse_coll_to_list(filepath, rest=False):
	import re
	x = []
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > 0:
				if rest:
					x.append(list(map(int,items[1:])))
				else:
					x.append(items[0])
	return x

def load_data():
	data = parse_coll("./DATA/IDM_RAW.txt", key=1)
	for k,v in parse_coll("./DATA/EA_RAW.txt", key=1).items():
		data[k] = v
	return data

def jaccard(a, b):
  a = set(list(a))
  b = set(list(b))
  return float(len(a.intersection(b))) / len(a.union(b))


if __name__ == "__main__":

  with open("corpus_stats.csv", "w") as csvfile:
    w = csv.writer(csvfile)
    w.writerow(["corpus", "alphabet_size"] + ["efficiency_{}".format(i+1) for i in range(10)] + ["repetition_{}".format(i+1) for i in range(10)] + ["mean_jaccard"])

    data = load_data()
    for corpus in ["IDM", "EA"]:
      for subcorpus in [0,1,2,3,4,5,10]:
        data_path = "./DATA/{}_{}.txt".format(corpus, subcorpus)
        if os.path.exists(data_path):
          subdata = [data[k] for k in np.unique(parse_coll_to_list(data_path))]
          
          if subcorpus == 10:
            subcorpus = "ALL"
          row = []
          row.append( "{}_{}".format(corpus, subcorpus) )
          row.append( alphabet(subdata) )
          for i in range(1,11):
            row.append( efficiency(subdata, i) )
          for i in range(1,11):
            row.append( repetition(subdata, i) )
          row.append( mean_jaccard(subdata) )

          w.writerow(row)
    
    txtdata = load_text_data()
    row = []
    row.append( "Text" )
    row.append( alphabet(txtdata) )
    for i in range(1,11):
      row.append( efficiency(txtdata, i) )
    for i in range(1,11):
      row.append( repetition(txtdata, i) )
    row.append( mean_jaccard(txtdata) )
    w.writerow(row)
  







