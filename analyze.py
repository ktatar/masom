import os
import operator
import numpy as np
np.warnings.filterwarnings('ignore')
from tqdm import tqdm
from scipy.stats import sem
import pickle
import itertools

def parse_coll(filepath, key=1):
	import re
	x = {}
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > key+1:
				x[items[key]] = list(map(int,items[key+1:]))
	return x

def parse_coll_to_list(filepath, rest=False):
	import re
	x = []
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > 0:
				if rest:
					x.append(list(map(int,items[1:])))
				else:
					x.append(items[0])
	return x

def parse_text_data():
	data = []
	alphabet = {}
	with open("t8.shakespeare.txt", "r") as file:
		for line in file.readlines():
			for c in line:
				data += [ord(c)]
				alphabet[ord(c)] = True
	# or ...
	r = np.random.randint(len(data) - 10000)
	data = np.array(data)[r:r+10000]
	return data, len(np.unique(data))
	return np.asarray(data), len(alphabet)

def windows(x,w):
	x = np.atleast_1d(x)
	return x[np.arange(x.shape[0]-w+1)[:,None] + np.arange(w)[None,:]]

def n_gram_plagarism(data, gen, n=3):
	# both data and gen are lists
	d = np.unique(np.concatenate([windows(x,n) for x in data],axis=0),axis=0)
	out = []
	for x in gen:
		copies = (d[None,:,:] == windows(x,n)[:,None,:]).all(2).any(1)
		assert len(copies) == len(x)-n+1
		out.append(np.mean(copies))
	return out

def n_gram_plagarism_defunct(data, gen, nmax=10):
	out = []
	for i in range(1,nmax+1):
		out.append( n_gram_plagarism_helper(data, gen, n=i) )
	return np.array(out)

def write_to_file(filepath, x):
	from array import array
	# write an integer array / list to a binary file
	with open(filepath, 'wb') as f:
		array('i', list(x)).tofile(f)

def read_from_file(filepath):
	from array import array
	# read an integer array from a binary file
	x = array('i')
	with open(filepath, 'rb') as f:
		success = True
		while success:
			try:
				x.fromfile(f,100)
			except:
				success = False
	return np.array(x)

def lcs(a,b):
	from subprocess import call
	import tempfile
	ap,bp,cp = ["./tmp/" + next(tempfile._get_candidate_names()) for i in range(3)]
	write_to_file(ap, a)
	write_to_file(bp, b)
	call(["./a.out", ap, bp, cp])
	c = read_from_file(cp)
	call(["rm", ap])
	call(["rm", bp])
	call(["rm", cp])
	return c

def longest_subsequence(data, gen):
	m = np.max([len(x) for x in data])
	return [int(np.max([len(lcs(d,g)) for d in data])) for g in gen]

def repetition(data, gen, n):
	if gen is None:
		x = data
	else:
		x = gen
	ngrams = np.concatenate([windows(xx,n) for xx in x], axis=0)
	return [1. - (float(len(np.unique(ngrams,axis=0))) / len(ngrams))]

def sample(a,n):
	return np.random.choice(a,size=(n,))

def load_data():
	data = parse_coll("./DATA/IDM_RAW.txt", key=1)
	for k,v in parse_coll("./DATA/EA_RAW.txt", key=1).items():
		data[k] = v
	return data

def eval_func(args):
	model, corpus, subcorpus, func, *extra = args
	gen_path = "./GEN/{}_{}_{}.npz".format(model, corpus, subcorpus)
	data_path = "./DATA/{}_{}.txt".format(corpus, subcorpus)
	isok = (model == "RANDOM") or (model is None)

	if (os.path.exists(gen_path) or isok) and os.path.exists(data_path):
		data = load_data()
		# remove dups for IDM ALL
		subdata = [data[k] for k in np.unique(parse_coll_to_list(data_path))]
		if model == "RANDOM":
			alphabet = np.unique(np.hstack(subdata))
			gen = np.random.choice(alphabet, size=(1000,2000))
		elif model == None:
			gen = None
		else:
			gen = np.load(gen_path)["data"][:1000]
		x = func(subdata, gen, *extra)

		return {'model': model, 'corpus': corpus, 'subcorpus' : subcorpus, "func" : func.__name__, "out" : list(x), "extra" : extra}

if __name__ == "__main__":

	# salloc --time=1:0:0 --ntasks=8 --account=def-pasquier --mem-per-cpu=4000M
	# salloc --time=0:10:0 --ntasks=8 --account=def-pasquier --mem-per-cpu=4000M
	# 1. determining an optimal performance via statistics from a set of samples
	# 2. studying the distribution of

	import itertools
	import os
	import random
	from subprocess import call
	from multiprocessing import Pool
	from tinydb import TinyDB, Query

	os.system("taskset -p 0xFFFFFFFF %d" % os.getpid()) # to run on all cores

	call(["rm", "a.out"])
	call(["gcc", "lcs.c"])
	#call(["rm", "eval.json"])
	call(["mkdir", "tmp"])
	db = TinyDB('eval.json')

	models = ["FACTOR", "VMM_3rdOrder", "VMM_5thOrder", "PPMC_3rdOrder", "PPMC_5thOrder", "RNN", "RANDOM"]
	corpora = ["EA", "IDM"]
	subcorpora = list(range(6)) + [9, 10]

	pool = Pool(processes=8)

	iter = list(itertools.product(
		models, corpora, subcorpora, [longest_subsequence]))
	iter += list(itertools.product(
		models, corpora, subcorpora, [n_gram_plagarism], range(1,11)))
	iter += list(itertools.product(
		[None], corpora, subcorpora, [repetition], range(1,11)))
	iter += list(itertools.product(
		models, corpora, subcorpora, [repetition], range(1,11)))

	# remove already discovered solutions
	fiter = []
	for item in tqdm(iter):
		model, corpus, subcorpus, func, *extra = item
		if len(db.search((Query().model == model) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus) & (Query().func == func.__name__) & (Query().extra == extra))) == 0:
			fiter.append(item)

	random.shuffle(fiter)

	for out in tqdm(pool.imap_unordered(eval_func, fiter), total=len(fiter)):
		if out is not None:
			db.insert(out)

	"""
	from subprocess import call
	call(["gcc", "lcs.c"])

	data = parse_coll("./DATA/IDM_RAW.txt", key=1)
	for k,v in parse_coll("./DATA/EA_RAW.txt", key=1).items():
		data[k] = v

	eval_funcs = [n_gram_plagarism, longest_subsequence]
	eval_funcs_dict = {f.__name__ : f for f in eval_funcs}

	data_funcs = [repetition]
	data_funcs_dict = {f.__name__ : f for f in data_funcs}

	txtdata, txtsize = parse_text_data()

	models = ["FACTOR", "VMM"] #["RNN", "FACTOR", "VMM"]
	datasets = ["EA", "IDM"]
	subsets = list(range(5))

	results = {}
	"""

	"""
	# tests on the generated material
	iter = itertools.product(models, datasets, subsets)
	for model, corpus, subcorpus in tqdm(list(iter)):
		gen_path = "./GEN/{}_{}_{}.txt".format(model, corpus, subcorpus)
		data_path = "./DATA/{}_{}.txt".format(corpus, subcorpus)
		corpus_name = "{}_{}".format(corpus, subcorpus)

		subdata = [data[k] for k in parse_coll_to_list(data_path)]
		gen = parse_coll_to_list(gen_path, rest=True)

		#for k,v in tqdm(eval_funcs_dict.items(), leave=False):
		#	if not k in results:
		#		results[k] = {}
		#	results[k][(model, corpus, subcorpus)] = v(subdata, gen)

		for k,v in tqdm(data_funcs_dict.items(), leave=False):
			if not k in results:
				results[k] = {}
			if not corpus_name in results[k]:
				results[k][corpus_name] = v(subdata)
			#if not ("RANDOM", subcorpus) in results[k]:
			#	a = len(np.unique(np.hstack(subdata)))
			#	results[k][("RANDOM", subcorpus)] = v([sample(a, 10000)])
			if not "TEXT" in results[k]:
				results[k]["RANDOM_TEXT"] = v([sample(txtsize, 10000)])
				results[k]["TEXT"] = v([txtdata])

	# save the results
	with open("EVAL_RESULTS.b", "wb") as f:
		pickle.dump(results, f)
	"""

	"""
	# produce the repetition plot
	from matplotlib import pyplot as plt

	linestyles = [

	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},

	    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
	    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'}
	]

	keys, data = zip(*sorted(results["repetition"].items()))
	data = data[:5] + data[-2:]
	keys = keys[:5] + keys[-2:]

	fig,ax = plt.subplots(1)
	for i,linestyle in enumerate(linestyles):
		if i < len(data):
			ax.plot(np.arange(1,11) + (np.random.rand(10) - .5) * 0, data[i], label=keys[i], markersize=6, **linestyle)
	ax.legend()
	plt.xlabel("ngram length")
	plt.show()

	keys, data = zip(*sorted(results["repetition"].items()))
	data = data[5:]
	keys = keys[5:]

	fig,ax = plt.subplots(1)
	for i,linestyle in enumerate(linestyles):
		if i < len(data):
			ax.plot(np.arange(1,11) + (np.random.rand(10) - .5) * 0, data[i], label=keys[i], markersize=6, **linestyle)
	ax.legend()
	plt.xlabel("ngram length")
	plt.show()


	exit()

	MAX_PATTERN_LENGTH = 10

	with PdfPages('longest_subsequence.pdf') as pdf:

		results = {}
		for corpus in GENERATED_DATA_PATHS.keys():
			TRAIN_DATA_PATH = TRAINING_DATA_PATHS[corpus]
			train = parse_coll(TRAIN_DATA_PATH, offset=2)
			train = np.concatenate(train)

			for algorithm, GEN_DATA_DIR in GENERATED_DATA_PATHS[corpus].items():

				gen = reduce(operator.add, [parse_coll(os.path.join(GEN_DATA_DIR,fp)) for fp in os.listdir(GEN_DATA_DIR)])

				key = "{}_{}".format(algorithm, corpus)
				results[key] = np.zeros((len(gen),))
				for i,g in enumerate(tqdm(gen)):
					#length = max(*[lcs_c(t,g) for t in train])
					length = lcs_c(train,g)
					print( length, len(g) )
					results[key][i] = float(length)

		keys = results.keys()
		colors = ['tab:blue', 'tab:red', 'tab:green', 'tab:orange']
		max_value = max(*[np.max(v) for v in results.values()])
		min_value = min(*[np.min(v) for v in results.values()])
		bins = np.linspace(min_value, max_value+1, 25)
		plt.figure(figsize=(10, 10))
		for k,color in zip(keys, colors):
			plt.hist(results[k], bins=bins, normed=True, color=color, alpha=0.75, label=k)
		plt.xlabel("normalized longest subsequence length")
		plt.legend(loc='upper left')
		pdf.savefig()
		plt.close()

	exit()


	with PdfPages('plagarism_max_{}.pdf'.format(MAX_PATTERN_LENGTH)) as pdf:

		for corpus in GENERATED_DATA_PATHS.keys():
			TRAIN_DATA_PATH = TRAINING_DATA_PATHS[corpus]
			train = parse_coll(TRAIN_DATA_PATH, offset=2)

			for algorithm, GEN_DATA_DIR in GENERATED_DATA_PATHS[corpus].items():

				gen = reduce(operator.add, [parse_coll(os.path.join(GEN_DATA_DIR,fp)) for fp in os.listdir(GEN_DATA_DIR)])

				train_patterns = PatternDict(train, max_pattern_length=MAX_PATTERN_LENGTH)

				results = np.zeros((len(gen),MAX_PATTERN_LENGTH))
				for i,g in enumerate(tqdm(gen)):
					gen_patterns = PatternDict([g], max_pattern_length=MAX_PATTERN_LENGTH)
					for n in range(1,MAX_PATTERN_LENGTH+1):
						results[i,n-1] = float(train_patterns.intersection(gen_patterns,n=n)) / (len(g)-n+1)

				# make a plot with error bars
				mu = np.mean(results, axis=0)
				err = sem(results)
				ub = mu + err
				lb = mu - err

				plt.figure(figsize=(10, 10))
				plt.plot(np.arange(MAX_PATTERN_LENGTH)+1, mu, color='tab:red')
				plt.plot(np.arange(MAX_PATTERN_LENGTH)+1, ub, ':', color='black')
				plt.plot(np.arange(MAX_PATTERN_LENGTH)+1, lb, ':', color='black')
				plt.ylabel("percentage plagarism")
				plt.xlabel("pattern length")
				plt.xlim([1,MAX_PATTERN_LENGTH])
				plt.ylim([0., 1.])
				plt.xticks(list(range(1, MAX_PATTERN_LENGTH+1)))
				plt.title("Algorithm : {}, Corpus : {}".format(algorithm, corpus))
				#plt.show()
				pdf.savefig()
				plt.close()
	"""


#
