#!/bin/bash
#SBATCH --account=def-pasquier
#SBATCH --cpus-per-task=8         # CPU cores/threads
#SBATCH --mem=4000M               # memory per node
#SBATCH --time=0-8:00            # time (DD-HH:MM)
#SBATCH --output=%N-%j.out  # %N for node name, %j for jobID
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

cd ~
module load python/3.5
source ~/MASOMENV/bin/activate
cd masom
python analyze.py
#source rnn_train.sh
