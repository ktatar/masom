from __future__ import print_function
import collections
import os
import re
import csv
import argparse
import operator
from functools import reduce
import numpy as np
np.warnings.filterwarnings('ignore')
from tqdm import tqdm

import tensorflow as tf
from keras.models import Sequential, load_model, Model
from keras.layers import Dense, Activation, Embedding, Dropout, TimeDistributed
from keras.layers import LSTM
from keras.layers import Masking, Input
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
import keras.backend as K

# maximum epochs (100) or minimum improvement (0.001)
# no validation set
# no testing set
# 10,000 sequences
# length 2000 sequences
# preprocess to remove empty nodes

def windows(x,w):
	x = np.atleast_1d(x)
	return x[np.arange(x.shape[0]-w+1)[:,None] + np.arange(w)[None,:]]

def parse_coll(filepath, key=1):
	x = {}
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > key+1:
				x[items[key]] = list(map(int,items[key+1:]))
	return x

def parse_corpus_list(filepath):
	x = []
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > 0:
				x.append(items[0])
	return x

def remove_unused_tokens(sequences):
	uniq = np.sort(np.unique(np.hstack(sequences)))
	mapping = collections.OrderedDict(zip(uniq, np.arange(len(uniq))))
	reverse_mapping = collections.OrderedDict(zip(np.arange(len(uniq)), uniq))
	sequences = [[mapping[x] for x in sequence] for sequence in sequences]
	return len(uniq), sequences, mapping, reverse_mapping

def pad(x, maxlen):
	out = np.zeros((maxlen,), dtype=np.int32)
	out[:len(x)] = x
	return out, (np.arange(maxlen) < len(x))

def split(x, maxlen, n_classes=None, window=False):
	out = []
	if window:
		for xx in windows(x,maxlen):
			if n_classes is not None:
				xx = to_categorical(xx, num_classes=n_classes)
			else:
				xx += 1
			out.append(xx)
	else:
		for i in range(0,len(x),maxlen):
			xx,b = pad(x[i:i+maxlen], maxlen)
			if n_classes is not None:
				xx = to_categorical(xx, num_classes=n_classes) * b[:,None]
			else:
				xx[b] += 1
			out.append(xx)
	return out

def create_padded_batches(x, maxlen=100, n_classes=100):
	train_inputs = reduce(
		operator.add, [split(xx[:-1],maxlen) for xx in x])
	train_outputs = reduce(
		operator.add, [split(xx[1:],maxlen,n_classes=n_classes) for xx in x])

	order = np.argsort(np.random.rand(len(train_inputs)))

	return np.array(train_inputs)[order], np.array(train_outputs)[order]

	"""
	vx = [xx[-1 * vlen:] for xx in x if len(xx) > vlen + maxlen]
	x = [xx[:-1 * vlen] for xx in x if len(xx) > vlen + maxlen]
	train_inputs = reduce(
		operator.add, [split(xx[:-1],maxlen) for xx in x])
	train_outputs = reduce(
		operator.add, [split(xx[1:],maxlen,n_classes=n_classes) for xx in x])
	valid_inputs = vx
	valid_outputs = [to_categorical(xx, num_classes=n_classes) for xx in vx]
	train_order = np.argsort(np.random.rand(len(train_inputs)))
	valid_order = np.argsort(np.random.rand(len(valid_inputs)))
	return (
		np.asarray(train_inputs)[train_order],
		np.asarray(train_outputs)[train_order],
		np.asarray(valid_inputs)[valid_order],
		np.asarray(valid_outputs)[valid_order]
	)
	"""


if __name__ == "__main__":

	window = False
	maxlen = 50
	vlen = 20
	batch_size = 32
	hidden_dim = 256
	embed_dim = 64
	use_dropout = True
	num_epochs = 50
	patience = 5
	generated_length = 2000
	num_examples = 1000

	parser = argparse.ArgumentParser()
	parser.add_argument("--subcorpus", default=0, type=int)
	parser.add_argument("--corpus", default="EA", type=str)
	parser.add_argument("--csv_path", required=True, type=str)
	args = parser.parse_args()

	name = "RNN_{}_{}".format(args.corpus, args.subcorpus)

	TRAIN_DATA_PATH = "./DATA/{}_RAW.txt".format(args.corpus)
	CORPUS_PATH = "./DATA/{}_{}.txt".format(args.corpus, args.subcorpus)

	valid_keys = None
	if os.path.exists(CORPUS_PATH):
		valid_keys = parse_corpus_list(CORPUS_PATH)

	raw_data = parse_coll(TRAIN_DATA_PATH, key=1)
	if valid_keys is not None:
		valid_keys = list(np.unique(valid_keys)) # remove dups
		raw_data = {k:v for k,v in raw_data.items() if k in valid_keys}
		assert len(raw_data) == len(valid_keys)
	else:
		print("WARNING :: TRAINING ON COMPLETE DATASET")

	n_classes, raw_data, mapping, rev_mapping = remove_unused_tokens(
		list(raw_data.values()))
	inputs, outputs = create_padded_batches(
		raw_data, maxlen=maxlen, n_classes=n_classes)

	n_valid = 3
	print(inputs.shape)
	ridx = np.argsort(np.random.rand(len(inputs)))
	valid_inputs = inputs[ridx[:n_valid]]
	valid_outputs = outputs[ridx[:n_valid]]
	inputs = inputs[ridx[n_valid:]]
	outputs = outputs[ridx[n_valid:]]

	model = Sequential()
	model.add(Embedding(n_classes+1, embed_dim, mask_zero=True))
	model.add(LSTM(hidden_dim, return_sequences=True))
	#model.add(LSTM(hidden_dim, return_sequences=True))
	if use_dropout:
		model.add(Dropout(0.5))
	model.add(TimeDistributed(Dense(n_classes)))
	model.add(Activation('softmax'))
	optimizer = Adam()
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])

	print(model.summary())

	#callbacks = [EarlyStopping(monitor='val_loss', min_delta=0, patience=patience, verbose=1, mode='auto', restore_best_weights=True)]
	from keras.callbacks import History 
	callbacks = [History()]

	model.fit(x=inputs, y=outputs, epochs=num_epochs, batch_size=batch_size, callbacks=callbacks, validation_data=(valid_inputs,valid_outputs))

	history = callbacks[0].history
	with open(args.csv_path, "w") as f:
		w = csv.writer(f)
		w.writerow(["epoch_num", "loss", "val_loss"])
		for n,(loss,val_loss) in enumerate(zip(history['loss'], history['val_loss'])):
			w.writerow([n,loss,val_loss])

	"""
	from matplotlib import pyplot as plt
	plt.plot(range(num_epochs), history['val_loss'], color='red')
	plt.plot(range(num_epochs), history['loss'], color='blue')
	plt.xlabel("num_epochs")
	plt.ylabel("loss")
	plt.show()
	"""

	# then produce some outputs using the model
	#output_choices = np.array([rev_mapping[i] for i in np.arange(n_classes)])
	primer = inputs[np.random.randint(len(inputs), size=(num_examples,))]
	samples = []
	for i in tqdm(range(generated_length)):

		pr = model.predict(primer)[:,-1,:]
		x = np.array([np.random.choice(np.arange(n_classes),p=p) for p in pr])
		primer = x[:,None]
		samples.append( np.array([rev_mapping[xx] for xx in x]) )

	samples = np.array(samples).T

	# write the samples to an .npz file
	np.savez_compressed("./GEN_TMP/{}.npz".format(name), data=samples)

	model.save("./MODEL/{}.hdf5".format(name))
