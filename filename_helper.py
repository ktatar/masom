# filename helper

import os
import numpy as np
from subprocess import call
import argparse

if __name__ == "__main__":

    arg_parser = argparse.ArgumentParser(description='Run tests')
    arg_parser.add_argument('--dry', default=1, type=int)
    args = arg_parser.parse_args()

    #dir = "/Users/Jeff/SFU/Phd/MASOM/GEN"
    dir = "./GEN"

    subcorpora = {"_{}.".format(i) : i for i in range(6)}
    corpora = {"Electro-acoustic" : "EA", "EA" : "EA", "IDM" : "IDM"}
    models = {"VMM" : "VMM", "factor" : "FACTOR", "FACTOR" : "FACTOR", "RNN" : "RNN"}

    def replace(x, d):
        for k,v in d.items():
            if k in x:
                return v

    for path in os.listdir(dir):
        fullpath = os.path.join(dir, path)
        if path.startswith(" "):
            #print(path, path[1:])
            call(["mv", os.path.join(dir, path), os.path.join(dir, path[1:])]) 
"""
if path.endswith(".txt"):
        model = replace(path, models)
        corpus = replace(path, corpora)
        subcorpus = replace(path, subcorpora)

        new_path = os.path.join(dir,"{}_{}_{}.txt".format(model, corpus, subcorpus))

        if not args.dry:
            call(["mv", path, new_path])
        elif path != new_path:
            print(path, new_path)
"""
