import numpy as np
import itertools
from scipy.stats import sem
from tinydb import TinyDB, Query
from matplotlib import pyplot as plt

db = TinyDB('eval.json')

models = ["VMM_3rdOrder", "VMM_5thOrder", "FACTOR", "PPMC_3rdOrder", "PPMC_5thOrder", "RNN", "RANDOM"]

model_names = ["V3", "V5", "FO", "P3", "P5", "RN", "RA"]
corpus_names = {
    0 : "0",
    1 : "1",
    2 : "2",
    3 : "3",
    4 : "4",
    5 : "5",
    9 : "ALL",
    10 : "ALL",
}

linestyles = [

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},

    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[4,4], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[8,8], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
]

line_dict = {(m,s) : style for (m,s),style in zip(itertools.product(models, range(6)), linestyles)}

model_line_dict = {m : linestyles[i*6] for i,m in enumerate(models)}

def longest_subsequence_graph(corpus, subcorpus, axis=None, max=700):
    func = "longest_subsequence"

    ax = plt
    if axis is not None:
        ax = axis


    dists = []
    labels = []
    for model, model_name in zip(models, model_names):
        d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
        print(len(d))

        if len(d) > 0:
            dists.append( d[0]["out"] )
            labels.append( model_name )

    ax.boxplot(dists, labels=labels)
    ax.set_ylim(0,max)
    if axis is None:
        plt.ylabel("length")
        plt.xlabel("model")
        plt.title("Longest Subsequence ({}, {})".format(corpus, subcorpus))
        plt.show()

def repetition_graph(corpus):
    func = "repetition"
    subcorpora = range(5) if corpus == "EA" else range(6)

    for model, subcorpus in itertools.product(models, subcorpora):
        d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
        d = sorted(d, key=lambda x:x["extra"])

        if len(d) > 0:

            x = np.array([xx["extra"][0] for xx in d])
            y = np.array([xx["out"] for xx in d])

            assert np.array_equal(x, np.arange(1,11))

            mu = np.mean(y,axis=1)
            #err = sem(y.T)

            plt.plot(x, mu, 'k-', **line_dict[(model,subcorpus)], label="{}_{}".format(model, subcorpus))

    plt.legend()
    plt.ylabel("length")
    plt.xlabel("model")
    plt.title("Repetition ({}, {})".format(corpus, subcorpus))
    plt.show()

def repetition_graph_subcorpus(corpus, subcorpus, axis=None):
    func = "repetition"

    ax = plt
    if axis is not None:
        ax = axis

    for model in models:
        d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
        d = sorted(d, key=lambda x:x["extra"])
        print(len(d))

        if len(d) > 0:

            x = np.array([xx["extra"][0] for xx in d])
            y = np.array([xx["out"] for xx in d])

            assert np.array_equal(x, np.arange(1,11))

            mu = np.mean(y,axis=1)
            #err = sem(y.T)

            plt.plot(x, mu, 'k-', **model_line_dict[model], label=model)

    if axis is None:
        plt.legend()
        plt.ylabel("length")
        plt.xlabel("model")
        plt.title("Repetition ({}, {})".format(corpus, subcorpus))
        plt.show()


def plagarism_graph(corpus):
    func = "n_gram_plagarism"
    subcorpora = range(5) if corpus == "EA" else range(6)

    for model, subcorpus in itertools.product(models, subcorpora):

        d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
        d = sorted(d, key=lambda x:x["extra"])

        if len(d) > 0:

            x = np.array([xx["extra"][0] for xx in d])
            y = np.array([xx["out"] for xx in d])

            assert np.array_equal(x, np.arange(1,11))

            mu = np.mean(y,axis=1)
            err = sem(y.T)

            plt.plot(x, mu, 'k-', **line_dict[(model,subcorpus)], label="{}_{}".format(model, subcorpus))

    plt.legend()
    plt.ylabel("plagarism percentage")
    plt.xlabel("ngram length")
    plt.title("Plagarism ({})".format(corpus))
    plt.show()

def plagarism_graph_subcorpus(corpus, subcorpus, axis=None):
    func = "n_gram_plagarism"

    ax = plt
    if axis is not None:
        ax = axis

    for model in models:

        d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
        d = sorted(d, key=lambda x:x["extra"])

        if len(d) > 0:

            x = np.array([xx["extra"][0] for xx in d])
            y = np.array([xx["out"] for xx in d])

            assert np.array_equal(x, np.arange(1,11))

            mu = np.mean(y,axis=1)
            err = sem(y.T)

            ax.plot(x[1:], mu[1:], 'k-', **model_line_dict[model], label=model)

    if axis is None:
        ax.legend()
        ax.ylabel("plagarism percentage")
        ax.xlabel("ngram length")
        ax.title("Plagarism ({})".format(corpus))
        ax.show()


if __name__ == "__main__":
    from matplotlib.backends.backend_pdf import PdfPages

    """
    # longest subsequence table put in csv
    import pandas as pd
    import functools
    from operator import add
    corpus = "IDM"
    func = "longest_subsequence"
    subcorpora = [0,1,2,3,4,5,"A"]
    RR = []
    for model, model_name in zip(models, model_names):
        R = []
        for subcorpus in [0,1,2,3,4,5,10]:
            d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
            if (len(d) > 0):
                R += [np.around(np.mean(d[0]["out"]),3), np.around(np.std(d[0]["out"]),3)]
            else:
                R += [0, 0]
        RR.append( R )

    columns = functools.reduce(add, [["C{} MEAN".format(i), "C{} STDEV".format(i)] for i in subcorpora])
    pd.DataFrame(
        data=RR,
        columns=columns).to_csv("idm.csv")

    corpus = "EA"
    func = "longest_subsequence"
    RR = []
    subcorpora = [0,1,2,3,4,"A"]
    for model, model_name in zip(models, model_names):
        R = []
        for subcorpus in [0,1,2,3,4,9]:
            d = db.search((Query().model == model) & (Query().func == func) & (Query().corpus == corpus) & (Query().subcorpus == subcorpus))
            if (len(d) > 0):
                R += [np.around(np.mean(d[0]["out"]),3), np.around(np.std(d[0]["out"]),3)]
            else:
                R += [0, 0]
        RR.append( R )

    columns = functools.reduce(add, [["C{} MEAN".format(i), "C{} STDEV".format(i)] for i in subcorpora])
    pd.DataFrame(
        data=RR,
        columns=columns,
        index={i:name for i,name in enumerate(models)}
    ).to_csv("ea.csv")

    exit()
    """

    with PdfPages('graph_EA_PLAGARISM.pdf') as pdf:

        # EA plagarism plot
        plt.figure(figsize=(20,4))
        plt.suptitle("EA PLAGARISM")
        for k,i in enumerate(list(range(5)) + [9]):
            ax = plt.subplot(1,6,k+1)
            plagarism_graph_subcorpus("EA", i, axis=ax)
            plt.xticks([2,3,4,5,6,7,8,9,10])
            plt.title("CORPUS_{}".format(corpus_names[i]))
            ax.set_ylim(-0.01,0.25)
            if k == 0:
                plt.ylabel("plagarism percentage")
            if k > 0:
                plt.yticks([])
            if k == 3:
                ax.legend(loc='upper center', bbox_to_anchor=(-0.1, -0.05),  shadow=False, frameon=False, ncol=len(models))
        pdf.savefig()
        plt.close()

    with PdfPages('graph_IDM_PLAGARISM.pdf') as pdf:

        # IDM plagarism plot
        plt.figure(figsize=(20,4))
        plt.suptitle("IDM PLAGARISM")
        for k,i in enumerate(list(range(6)) + [9]):
            ax = plt.subplot(1,7,k+1)
            plagarism_graph_subcorpus("IDM", i, axis=ax)
            plt.xticks([2,3,4,5,6,7,8,9,10])
            plt.title("CORPUS_{}".format(corpus_names[i]))
            ax.set_ylim(-0.0025,0.075)
            if k == 0:
                plt.ylabel("plagarism percentage")
            if k > 0:
                plt.yticks([])
            if k == 3:
                ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),  shadow=False, frameon=False, ncol=len(models))
        pdf.savefig()
        plt.close()

    EA = [[0.8278008298755186, 0.3544568245125348, 0.1486676016830295, 0.08898305084745761, 0.061166429587482196, 0.045128939828080195, 0.03246753246753242,0.02470930232558144, 0.019765739385065872, 0.016961651917404175], [0.8080517613227893, 0.40593342981186686, 0.1828113619810634, 0.08870967741935487, 0.05092250922509223, 0.03194650817236255, 0.022438294689603566, 0.015060240963855387, 0.009855951478392688, 0.006870229007633566], [0.8309800664451827, 0.3251354731137974, 0.1251046025104603, 0.07013859722805549, 0.04637436762225966, 0.033432077867118104, 0.02548853016142738, 0.019616204690831585, 0.015410958904109595, 0.01203266007735282], [0.8525194592380172, 0.1418002466091245, 0.02268041237113405, 0.009102192800993003, 0.00498132004981322, 0.002498958767180315, 0.001253656498119482, 0.00041928721174000483, 0.0, 0.0], [0.8152992469282601, 0.37225986448784376, 0.15911823647294587, 0.08464328899637241, 0.04783137413862992, 0.027313493681206724, 0.018860188601885985, 0.014432989690721598, 0.010783907092492773, 0.008760951188986232], [0.8529728670780684, 0.31006988876857955, 0.1206845385300227, 0.06432050899691821, 0.039464481966230425, 0.025906215483482264, 0.018568977697043132, 0.013794502485038995, 0.010397553516819591, 0.008298330089130168]]

    IDM = [[0.8182527301092044, 0.41111981205951453, 0.20204402515723274, 0.11444356748224149, 0.07606973058637079, 0.05250596658711215, 0.03913738019169333,0.03287890938251803, 0.027375201288244777, 0.023443815683104274], [0.8387839705204975, 0.4810710987996306, 0.3267006015733457, 0.24721706864564008,0.19618781961878196, 0.16029822926374648, 0.1303129378794956, 0.10767790262172283, 0.08869075551384331, 0.07337723424270937], [0.8604972375690607, 0.47539847539847535, 0.2579972183588317, 0.16608513607815767, 0.11764705882352944, 0.08784258608573436, 0.0691114245416079, 0.05449398443029019, 0.04190340909090906, 0.03207412687099076], [0.8186304750154225, 0.4742396027312229, 0.24734540911930047, 0.13136392206159653, 0.07337128399746995, 0.04010184595798849, 0.021780909673286386, 0.015473887814313358, 0.009733939000648895, 0.0052253429131287055], [0.8353153153153153, 0.40687160940325495,0.2192377495462795, 0.13734061930783237, 0.09652650822669107, 0.07449541284403671, 0.05854197349042711, 0.047654229774658274, 0.040400296515937684,0.03421346225362587], [0.837698139214335, 0.41528354080221297, 0.20680083275503125, 0.12883008356545966, 0.08211041229909155, 0.05189340813464238, 0.032371569317382165, 0.02153954802259883, 0.015237420269312518, 0.011379800853485111], [0.8631681897368502, 0.4508367559615031, 0.2566009897642394,0.18569667077681873, 0.15009119856555475, 0.12767408693495386, 0.11177787451029164, 0.10059245400686001, 0.09240728000500342, 0.08539798030483603]]

    with PdfPages('graph_EA_REPETITION.pdf') as pdf:

        # EA repetition plot
        plt.figure(figsize=(20,4))
        plt.suptitle("EA REPETITION")
        for k,i in enumerate(list(range(5)) + [9]):
            ax = plt.subplot(1,6,k+1)
            repetition_graph_subcorpus("EA", i, axis=ax)
            plt.plot(range(1,11), EA[k], label="corpus", color="black", dashes=[2,2])
            plt.xticks([1,2,3,4,5,6,7,8,9,10])
            plt.title("CORPUS_{}".format(corpus_names[i]))
            ax.set_ylim(-0.05,1.05)
            if k == 0:
                plt.ylabel("repetition percentage")
            if k > 0:
                plt.yticks([])
            if k == 3:
                ax.legend(loc='upper center', bbox_to_anchor=(-0.1, -0.05),  shadow=False, frameon=False, ncol=len(models))
        pdf.savefig()
        plt.close()

    with PdfPages('graph_IDM_REPETITION.pdf') as pdf:

        # IDM repetition plot
        plt.figure(figsize=(20,4))
        plt.suptitle("IDM REPETITION")
        for k,i in enumerate(list(range(6)) + [9]):
            ax = plt.subplot(1,7,k+1)
            repetition_graph_subcorpus("IDM", i, axis=ax)
            plt.plot(range(1,11), IDM[k], label="corpus", color="black", dashes=[2,2])
            plt.xticks([1,2,3,4,5,6,7,8,9,10])
            plt.title("CORPUS_{}".format(corpus_names[i]))
            ax.set_ylim(-0.05,1.05)
            if k == 0:
                plt.ylabel("repetition percentage")
            if k > 0:
                plt.yticks([])
            if k == 3:
                ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),  shadow=False, frameon=False, ncol=len(models))
        pdf.savefig()
        plt.close()

    with PdfPages('graph_EA_LCS.pdf') as pdf:

        # EA longest_subsequence
        plt.figure(figsize=(20,4))
        plt.suptitle("EA LONGEST SUBSEQUENCE")
        for k,i in enumerate(list(range(5)) + [9]):
            ax = plt.subplot(1,6,k+1)
            longest_subsequence_graph("EA", i, axis=ax, max=10)
            plt.title("CORPUS_{}".format(corpus_names[i]))
            if k == 0:
                plt.ylabel("subsequence length")
            if k > 0:
                plt.yticks([])
        pdf.savefig()
        plt.close()

    with PdfPages('graph_IDM_LCS.pdf') as pdf:

        # IDM longest_subsequence
        plt.figure(figsize=(20,4))
        plt.suptitle("IDM LONGEST SUBSEQUENCE")
        for k,i in enumerate(list(range(6)) + [9]):
            ax = plt.subplot(1,7,k+1)
            longest_subsequence_graph("IDM", i, axis=ax, max=10)
            plt.title("CORPUS_{}".format(corpus_names[i]))
            if k == 0:
                plt.ylabel("subsequence length")
            if k > 0:
                plt.yticks([])
        pdf.savefig()
        plt.close()



#
