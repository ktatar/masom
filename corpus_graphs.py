from matplotlib import pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

linestyles = [
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'x', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'o', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'D', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'v', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'^', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'+', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'s', 'fillstyle':'none'},
    {'dashes':[2,2], 'lw':1, 'c':'black', 'marker':'*', 'fillstyle':'none'},
]

with open("corpus_stats.csv", "r") as f:
  data = list(f.readlines())


# efficicency IDM
with PdfPages('graph_corpus_IDM_efficiency.pdf') as pdf:
  plt.figure(figsize=(9,6.75))
  ax = plt.subplot(1,1,1)
  for i,ii in enumerate(list(range(1,8)) + [14]):
    line = data[ii].split(",")
    plt.plot(range(1,11), [float(x) for x in line[2:12]], **linestyles[i], label=line[0])

  plt.xticks(range(1,11))
  plt.ylim(0,1)
  plt.ylabel("efficiency")
  plt.xlabel("ngram")
  ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),  shadow=False, frameon=False, ncol=8, fontsize=10)
  plt.tight_layout()
  pdf.savefig()
  plt.close()

# efficiency corpus EA
with PdfPages('graph_corpus_EA_efficiency.pdf') as pdf:
  plt.figure(figsize=(9,6.75))
  ax = plt.subplot(1,1,1)
  for i,ii in enumerate(range(8,15)):
    lin = data[ii].split(",")
    plt.plot(range(1,11), [float(x) for x in lin[2:12]], **linestyles[i], label=lin[0])

  plt.xticks(range(1,11))
  plt.ylim(0,1)
  plt.ylabel("efficiency")
  plt.xlabel("ngram")
  ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),  shadow=False, frameon=False, ncol=7, fontsize=10)
  plt.tight_layout()
  pdf.savefig()
  plt.close()

# repetition IDM
with PdfPages('graph_corpus_IDM_repetition.pdf') as pdf:
  plt.figure(figsize=(9,6.75))
  ax = plt.subplot(1,1,1)
  for i,ii in enumerate(list(range(1,8)) + [14]):
    line = data[ii].split(",")
    plt.plot(range(1,11), [float(x) for x in line[12:22]], **linestyles[i], label=line[0])

  plt.xticks(range(1,11))
  plt.ylim(0,1)
  plt.ylabel("repetition")
  plt.xlabel("ngram")
  ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),  shadow=False, frameon=False, ncol=8, fontsize=10)
  plt.tight_layout()
  pdf.savefig()
  plt.close()

# repetition corpus EA
with PdfPages('graph_corpus_EA_repetition.pdf') as pdf:
  plt.figure(figsize=(9,6.75))
  ax = plt.subplot(1,1,1)
  for i,ii in enumerate(range(8,15)):
    lin = data[ii].split(",")
    plt.plot(range(1,11), [float(x) for x in lin[12:22]], **linestyles[i], label=lin[0])

  plt.xticks(range(1,11))
  plt.ylim(0,1)
  plt.ylabel("repetition")
  plt.xlabel("ngram")
  ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),  shadow=False, frameon=False, ncol=7, fontsize=10)
  plt.tight_layout()
  pdf.savefig()
  plt.close()

