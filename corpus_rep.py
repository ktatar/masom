# get repetition of corpus

import re
import numpy as np

def parse_coll(filepath, key=1):
	import re
	x = {}
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > key+1:
				x[items[key]] = list(map(int,items[key+1:]))
	return x

def parse_coll_to_list(filepath, rest=False):
	import re
	x = []
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > 0:
				if rest:
					x.append(list(map(int,items[1:])))
				else:
					x.append(items[0])
	return x

def windows(x,w):
	x = np.atleast_1d(x)
	return x[np.arange(x.shape[0]-w+1)[:,None] + np.arange(w)[None,:]]

def repetition(x,n):
	ngrams = np.concatenate([windows(xx,n) for xx in x], axis=0)
	return [1. - (float(len(np.unique(ngrams,axis=0))) / len(ngrams))]

data = parse_coll("./DATA/EA_RAW.txt")
EA = []
for k in [0,1,2,3,4,10]:
  titles = parse_coll_to_list("./DATA/EA_{}.txt".format(k))
  EA.append( [repetition([data[t] for t in titles], i)[0] for i in range(1,11)] )

print(EA)

data = parse_coll("./DATA/IDM_RAW.txt")
IDM = []
for k in [0,1,2,3,4,5,10]:
  titles = parse_coll_to_list("./DATA/IDM_{}.txt".format(k))
  IDM.append( [repetition([data[t] for t in titles], i)[0] for i in range(1,11)] )

print(IDM)