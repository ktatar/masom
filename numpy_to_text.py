import numpy as np
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("--inpath", type=str, required=True)
parser.add_argument("--outpath", type=str, required=True)
args = parser.parse_args()

assert os.path.exists(args.inpath)

with open(args.outpath, "w") as f:
  for ii,row in enumerate(np.load(args.inpath)["data"]):
    f.write(str(ii) + ", " + " ".join([str(x) for x in row]) + ";")
