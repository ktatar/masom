#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stddef.h>

#define MAX(a, b) (a > b ? a : b)

int *lcsFIN(int *a, int* b, int alen, int blen, int *olen) {
    size_t apos, bpos;

    int len = 0;
    int beg = 0;
    int end = 0;

    for (apos = 0; apos < alen; ++apos) {
        for (bpos = 0; bpos < blen; ++bpos) {
            if (a[apos] == b[bpos]) {
                len = 1;
                while (apos + len < alen && bpos + len < blen && a[apos + len] == b[bpos + len]) {
                    len++;
                }
            }

            if (len > end - beg) {
                beg = apos;
                end = beg + len;
                len = 0;
            }
        }
    }

    int *x = (int*)malloc(sizeof(int) * (end - beg));
    *olen = 0;
    for (int j = beg; j < end; j++) {
        x[*olen] = a[j];
        *olen = *olen + 1;
    }
    return x;
}

int LCSubStr(int* X, int* Y, int m, int n, int* r)
{
    int LCSuff[m + 1][n + 1];
    int len = 0;
    int row, col;

    for (int i = 0; i <= m; i++) {
        for (int j = 0; j <= n; j++) {
            if (i == 0 || j == 0)
                LCSuff[i][j] = 0;

            else if (X[i - 1] == Y[j - 1]) {
                LCSuff[i][j] = LCSuff[i - 1][j - 1] + 1;
                if (len < LCSuff[i][j]) {
                    len = LCSuff[i][j];
                    row = i;
                    col = j;
                }
            }
            else
                LCSuff[i][j] = 0;
        }
    }

    if (len == 0) {
      return 0;
    }

    r = (int*)malloc((len + 1) * sizeof(int));

    // traverse up diagonally form the (row, col) cell
    // until LCSuff[row][col] != 0
    while (LCSuff[row][col] != 0) {
        r[--len] = X[row - 1]; // or Y[col-1]

        // move diagonally up to previous cell
        row--;
        col--;
    }
    return len;
}

int LongestCommonSubstring(int *a, int n, int *b, int m)
{
     if ((n == 0) | (m == 0)) {
       return 0;
     }

     int *curr = malloc(sizeof(int)*m);
     int *prev = malloc(sizeof(int)*m);
     int *swap = NULL;
     int maxSubstr = 0;

     for(int i = 0; i<n; ++i) {
          for(int j = 0; j<m; ++j) {
               if(a[i] != b[j]) {
                    curr[j] = 0;
               }
               else {
                    if(i == 0 || j == 0) {
                         curr[j] = 1;
                    }
                    else {
                         curr[j] = 1 + prev[j-1];
                    }
                    maxSubstr = MAX(maxSubstr, curr[j]);
               }
          }
          swap=curr;
          curr=prev;
          prev=swap;
     }
     free(curr);
     free(prev);
     return maxSubstr;
}

int lcs (int *a, int n, int *b, int m, int **s) {
    int i, j, k, t;
    int *z = calloc((n + 1) * (m + 1), sizeof (int));
    int **c = calloc((n + 1), sizeof (int *));
    if ((z == NULL) || (c == NULL)) {
      return 0;
    }
    for (i = 0; i <= n; i++) {
        c[i] = &z[i * (m + 1)];
    }
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            if (a[i - 1] == b[j - 1]) {
                c[i][j] = c[i - 1][j - 1] + 1;
            }
            else {
                c[i][j] = MAX(c[i - 1][j], c[i][j - 1]);
            }
        }
    }
    t = c[n][m];
    *s = malloc(t*sizeof(int));
    for (i = n, j = m, k = t - 1; k >= 0;) {
        if (a[i - 1] == b[j - 1])
            (*s)[k] = a[i - 1], i--, j--, k--;
        else if (c[i][j - 1] > c[i - 1][j])
            j--;
        else
            i--;
    }
    free(c);
    free(z);
    return t;
}

int *read_binary(const char *filepath, int *len) {
  FILE* f = fopen(filepath, "rb" );
	fseek(f, 0, SEEK_END);
	*len = ftell(f)/4;
  fseek(f, 0, SEEK_SET);
	int *x = malloc( sizeof(int)*(*len) );
  if (x == NULL) {
    return 0;
  }
  size_t check = fread(x, sizeof(int), *len, f);
	assert (check == *len);
  fclose(f);
  return x;
}

int write_binary(const char* filepath, int* x, int len) {
  FILE* f = fopen( filepath, "wb" );
  fseek(f, 0, SEEK_SET);
	size_t check = fwrite( x, sizeof(int), len, f );
	if (check != len)
		return 1;
	fclose( f );
	return 0;
}

int main (int argc, char **argv) {

  int alen, blen, clen;
  int *a = read_binary(argv[1], &alen);
  int *b = read_binary(argv[2], &blen);

  int *c = lcsFIN(a, b, alen, blen, &clen);
  int r = write_binary(argv[3], c, clen);

}
