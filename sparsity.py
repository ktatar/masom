import os
import numpy as np
import itertools
import operator
import functools
#import nltk
#nltk.download('gutenberg')
#from nltk.tokenize import RegexpTokenizer

def windows(x,w):
	x = np.atleast_1d(x)
	return x[np.arange(x.shape[0]-w+1)[:,None] + np.arange(w)[None,:]]

def sparsity(x,w):
  ng = np.unique(np.concatenate([windows(xx,w) for xx in x if len(xx) >= w],axis=0),axis=0)
  return np.mean((ng[:,1:][:-1,None,:] == ng[:,:-1][None,:,:]).all(2).sum(1))

def parse_coll(filepath, key=1):
	import re
	x = {}
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = re.findall(r'(?:[^\s,"]|"(?:\\.|[^"])*")+', line)
			if len(items) > key+1:
				x[items[key]] = list(map(int,items[key+1:]))
	return x

def load_text_data():
  tokens = []
  d = {}
  tokenizer = RegexpTokenizer(r'\w+')
  for path in nltk.corpus.gutenberg.fileids():
    raw = nltk.corpus.gutenberg.raw(path)
    words = list(tokenizer.tokenize(raw))
    token = []
    for w in words:
      if not w in d:
        d[w] = len(d)
      token.append(d[w])
    tokens.append(token[:1000])
  return tokens

if __name__ == "__main__":

  import argparse
  import csv
  parser = argparse.ArgumentParser()
  parser.add_argument("--data_path", type=str, required=True)
  parser.add_argument("--corpus_path", type=str, required=True)
  parser.add_argument("--csv_path", type=str, required=True)
  args = parser.parse_args()

  print("=" * 50)
  print(args.corpus_path)
  
  data = parse_coll(args.data_path)
  corpus = list(parse_coll(args.corpus_path,key=0).keys())
  data = {k : v for k,v in data.items() if k in corpus}
  data_no_repeats = {k : [x for x,_ in itertools.groupby(v)] for k,v in data.items()}
  alphabet_size = len(np.unique(functools.reduce(operator.add, list(data.values()))))
  random_data = {k : np.random.randint(alphabet_size, size=(len(v),)) for k,v in data.items()}

  with open(args.csv_path, "w") as f:
    w = csv.writer(f)
    w.writerow(["ngram_size", "raw_data", "raw_data_no_repeat", "random_data"])

    for i in range(2,10):
      res = [
        sparsity(list(data.values()),i), 
        sparsity(list(data_no_repeats.values()),i), 
        sparsity(list(random_data.values()),i)]
      w.writerow([i] + res)
      print(i, res)
          
