import os
import numpy as np

def parse_coll(filepath, rest=False):
	x = []
	with open(filepath, "r") as file:
		for line in file.read().split(";"):
			items = line.split()
			if len(items) > 0:
				x.append([int(item) for item in items[1:]])
	return x

if __name__ == "__main__":

	dir = "./GEN"
	for path in os.listdir(dir):
		if path.endswith(".txt"):
			path = os.path.join(dir, path)
			if not os.path.exists(os.path.splitext(path)[0] + ".npz"):
				print(path)
				x = np.asarray(parse_coll(path, rest=True), dtype=np.int32)[:1000]
				assert x.shape[0] == 1000
				assert x.shape[1] >= 2000
				np.savez_compressed(os.path.splitext(path)[0], data=x[:,:2000])
