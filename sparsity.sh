python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_0.txt --csv_path SPARSITY_EA_0.csv
python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_1.txt --csv_path SPARSITY_EA_1.csv
python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_2.txt --csv_path SPARSITY_EA_2.csv
python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_3.txt --csv_path SPARSITY_EA_3.csv
python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_4.txt --csv_path SPARSITY_EA_4.csv
python3 sparsity.py --data_path ./DATA/EA_RAW.txt --corpus_path ./DATA/EA_10.txt --csv_path SPARSITY_EA_10.csv

python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_0.txt --csv_path SPARSITY_IDM_0.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_1.txt --csv_path SPARSITY_IDM_1.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_2.txt --csv_path SPARSITY_IDM_2.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_3.txt --csv_path SPARSITY_IDM_3.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_4.txt --csv_path SPARSITY_IDM_4.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_5.txt --csv_path SPARSITY_IDM_5.csv
python3 sparsity.py --data_path ./DATA/IDM_RAW.txt --corpus_path ./DATA/IDM_10.txt --csv_path SPARSITY_IDM_10.csv